# coding=utf-8
#Libraries
import RPi.GPIO as GPIO
import time
import requests
import leds


URL_GETCOMMAND = 'https://api.thingspeak.com/talkbacks/28755/commands/execute?api_key=8EMOI0AEKVE4FJBD'

 
def checkCommand():
    
    r = requests.get(URL_GETCOMMAND)
    return r.text
 
if __name__ == '__main__':
    
   leds.init()
    
   try:
        while True:
            
            command = checkCommand()
            if command == 'warning':
                leds.ledOn()
            else:
                leds.ledOff()
                
            print('Command:', command)
                            
            time.sleep(5)

 
        # Reset by pressing CTRL + C
   except KeyboardInterrupt:
        print("Measurement stopped by User")
        
