import RPi.GPIO as GPIO
import time
import sys

led = 26

def init():
    print('init', led)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(led, GPIO.OUT)
    GPIO.output(led, GPIO.LOW)
    

def ledOn():
    print('led on',led)
    GPIO.output(led, GPIO.HIGH)
    
def ledOff():
    print('led off', led)
    GPIO.output(led, GPIO.LOW)
    

