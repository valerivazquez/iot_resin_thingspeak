# coding=utf-8
#Libraries
import RPi.GPIO as GPIO
import time
import requests
import leds

URL_GETCOMMAND = 'https://api.thingspeak.com/talkbacks/28755/commands/execute?api_key=8EMOI0AEKVE4FJBD'

URL_SENDDISTANCE = 'https://api.thingspeak.com/update?api_key=KNSL4GJPHJAMLER8'

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

BASE_DISTANCE = 180 # cm to check changes
BASE_DISTANCE_ERROR = 10 # % error allow to be in range


#set GPIO Pins
GPIO_TRIGGER = 4
GPIO_ECHO = 17
 
#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)
 
 
def sendThingSpeak(distance, out):
    
    paramDistance = '&field1=' + str(distance)
    paramOut = '&field2=' + str(out)
    r = requests.get(URL_SENDDISTANCE + paramDistance + paramOut)
 
def checkCommand():
    
    r = requests.get(URL_GETCOMMAND)
    return r.text





def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
 
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
 
    StartTime = time.time()
    StopTime = time.time()
    
 
    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0: #Mientras el sensor no reciba señal...
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1: #Si el sensor recibe señal...
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2
 
    return distance
 
if __name__ == '__main__':
    print ("===================== INIT PARAMETERS =========================================")
    print ("Base Measured Distance = %.1f cm" % BASE_DISTANCE)
    print ("Error allow = %.2f" % BASE_DISTANCE_ERROR, "%")
    BASE_DISTANCE_MAX = BASE_DISTANCE * (1 + (BASE_DISTANCE_ERROR / 100))
    BASE_DISTANCE_MIN = BASE_DISTANCE * (1 - (BASE_DISTANCE_ERROR / 100))
    print ("Base distance Max = %.2f cm" % BASE_DISTANCE_MAX)
    print ("Base distance Min = %.2f cm" % BASE_DISTANCE_MIN)
    print ("===============================================================================")
    
    leds.init()
    
    try:
        while True:
            dist = distance()
            if BASE_DISTANCE_MIN <= dist <= BASE_DISTANCE_MAX:
                sendThingSpeak(dist, 0)
                print ("Measured ALbert Distance in range = %.1f cm" % dist)
            else:
                sendThingSpeak(dist, 1)
                print ("Measured Albert Distance OUT range = %.1f cm" % dist)
                
            command = checkCommand()
            if command == 'warning':
                leds.ledOn()
            else:
                leds.ledOff()
                
            print('Command:', command)
            time.sleep(30)

 
 
        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
